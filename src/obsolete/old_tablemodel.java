/**
 * 
 */
package obsolete;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import model.StocksModel;


/**
 * @author alexander
 *
 */
public class old_tablemodel {

}
class MainTableModel implements TableModel {
	@Override
	public void addTableModelListener(TableModelListener l) {
	}

	/**
	 * Gibt eine Zelle als String zurueck.
	 */
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	/**
	 * Methode gibt an, wieviele Spalten erstellt werden muessen
	 */
	public int getColumnCount() {
		return StocksModel.getInstance().getHeader().length;
	}

	/**
	 * Methode erteilt den Spalten-Namen. Hierbei wird auf die erste Zeile der 
	 * CSV-Datei zurueckgegriffen.
	 */
	// TODO: Namen muessen eindeutig fuer den Benutzer sein.
	public String getColumnName(int columnIndex) {
		return StocksModel.getInstance().getHeader()[columnIndex];
	}

	/**
	 * Methode gibt an, wieviele Zeilen die Tabelle benötigt.
	 */
	public int getRowCount() {
		return StocksModel.getInstance().getStocksList().size();
	}

	/**
	 * Methode 
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0:
				return StocksModel.getInstance().getStocksList().get(rowIndex).getWkn();
			case 1:
				return StocksModel.getInstance().getStocksList().get(rowIndex).getName();
			case 2:
				return StocksModel.getInstance().getStocksList().get(rowIndex).getQuantity();
			case 3:
				return StocksModel.getInstance().getStocksList().get(rowIndex).getCurRate();
			case 4:
				return StocksModel.getInstance().getStocksList().get(rowIndex).getBuyDate();
			case 5:
				return StocksModel.getInstance().getStocksList().get(rowIndex).getBuyRate();
			case 6:
				return "SOLD";
			case 7:
				return StocksModel.getInstance().getStocksList().get(rowIndex).getSellDate();
			case 8:
				return StocksModel.getInstance().getStocksList().get(rowIndex).getSellRate();
			default:
				return "";
		}
	}

	/**
	 * Damit innerhalb der Tabelle keine Werte editiert werden koennen, wird hier false zurueckgegeben.
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
	}

	/**
	 * Methode die als Setter fuer einzelne Zellen gilt, d.h. es ist moeglich die Zelle direkt in der 
	 * Tabelle zu editieren. Funktion ist nur gegeben, wenn die Methode isCellEditable ein 
	 * true zurueckliefert.
	 */
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0: 
				StocksModel.getInstance().getStocksList().get(rowIndex).setWkn((String)value);
			case 1: 
				StocksModel.getInstance().getStocksList().get(rowIndex).setName((String)value);
		}
	}
}