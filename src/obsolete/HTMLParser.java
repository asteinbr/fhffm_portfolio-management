package obsolete;

/**
 * Simple HTMLParser
 * currently for debugging purposes
 */

import java.util.HashMap;
import java.io.*;

/**
 * @author alexander
 *
 */
public class HTMLParser {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader("HTMLTest"));
		
		boolean inData = false;
		String current_line;
		HashMap<String, String> name_value_list = new HashMap<String, String>();
		
		while (br.ready()) {
			current_line = br.readLine();
			System.out.println(current_line);
			
			if (current_line.contains("WKN")) {
				inData = true;
				name_value_list.put("WKN", current_line.split(" ")[1].substring(0, 6));
			} // if (current_line.contains("WKN"))
		} // while (br.ready())
		
		for (String key: name_value_list.keySet())
			System.out.println(key + " : " + name_value_list.get(key));

	} // public static void main(String[] args)
} // public class HTMLParser
