/**
 * 
 */
package view;

/**
 * @author alexander
 * 
 * Class - RunAllStocks
 * 
 * Diese Klasse startet eine Instanz des Hauptdialogs.
 */
public class RunAllStocks {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// CSV-Dateien importieren
		//StocksModel.getInstance().csvImport("BlueChips_.csv");

		// AllStocks-Fenster erzeugen
		AllStocks allStocks = new AllStocks(820, 500);
	}
}
