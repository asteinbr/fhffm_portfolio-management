/**
 * 
 */
package view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import model.StocksModel;
import model.Stock;

/**
 * @author alexander
 * 
 * Class - SellStock
 * 
 * Diese Klasse definiert den "Aktien verkaufen"-Dialog.
 * Nutzung des GridBag-Layouts.
 * Abfangen von Leer-Eingaben.
 */
public class SellStock extends JFrame implements ActionListener {
	private ArrayList<Stock> list = StocksModel.getInstance().getStocksList();
	private int selectedItem = 0;
	
	// Deklaration Textfelder
	private JTextField textFieldSellRate = new JTextField(7);
	private JTextField textFieldSellDate = new JTextField(14);
	
	private Container SellStockContainer(int selectedRow) {
		// GridBagLayout
		Container contentPane = getContentPane();
		GridBagLayout gridBag = new GridBagLayout();
		contentPane.setLayout(gridBag);
		
		// Deklaration der Labels
		JLabel labelInfo = new JLabel("Zum Verkauf Aktie auswählen:");
		
		// Deklaration der ComboBox
		JComboBox comboBox = new JComboBox();
			// ActionListener aktivieren
			comboBox.addActionListener(this);
		
		// Elemente aus der Aktien-Liste der comboBox hinzufügen
		for (int i = 0; i < list.size(); i++)
			comboBox.addItem(list.get(i).getName());
		
		// ComboBox auf das uebergebene Element setzen
		if (selectedRow >= 0)
			comboBox.setSelectedIndex(selectedRow);
		
		
		// Deklaration Label's
		JLabel labelSellRate = new JLabel("Verkaufs-Preis:");
		JLabel labelSellDate = new JLabel("Verkaufs-Datum:");
		
		JLabel labelDummySpace1 = new JLabel(" ");
		JLabel labelDummySpace2 = new JLabel(" ");
		JLabel labelDummySpace3 = new JLabel(" ");
		
		// Deklaration der Buttons
		JButton buttonSell = new JButton("Verkaufen");
		JButton buttonCancel = new JButton("Abbrechen");
		
		JButton buttonCurrentDate = new JButton("*");
			buttonCurrentDate.setToolTipText("aktuelles Datum einfügen");
			buttonCurrentDate.setPreferredSize(new Dimension(18, 18));
		
		/*
		 * Hinzufügen der einzelnen Elemente
		 * 
		 * Jedes Element braucht einen GridBagConstraint mit Details über
		 * die Positionierung
		 */
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(2, 2, 2, 2);
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.fill = GridBagConstraints.FIRST_LINE_START;
		gbc.anchor = GridBagConstraints.WEST;
		
		// labelInfo
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		gridBag.setConstraints(labelInfo, gbc);
		contentPane.add(labelInfo);

		// labelDummySpace
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridwidth = 3;
		gridBag.setConstraints(labelDummySpace1, gbc);
		contentPane.add(labelDummySpace1);
		
		// comboBox
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.gridwidth = 3;
		gbc.fill = GridBagConstraints.BOTH;
		gridBag.setConstraints(comboBox, gbc);
		contentPane.add(comboBox);
		
		gbc.fill = GridBagConstraints.FIRST_LINE_START;
		
		// labelDummySpace2
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.gridwidth = 3;
		gridBag.setConstraints(labelDummySpace2, gbc);
		contentPane.add(labelDummySpace2);
		
		// labelSellRate
		gbc.gridx = 1;
		gbc.gridy = 5;
		gridBag.setConstraints(labelSellRate, gbc);
		contentPane.add(labelSellRate);
		
		// textFieldSellRate
		gbc.gridx = 2;
		gbc.gridy = 5;
		gbc.gridwidth = 1;
		gridBag.setConstraints(textFieldSellRate, gbc);
		contentPane.add(textFieldSellRate);
		
		// labelSellDate
		gbc.gridx = 1;
		gbc.gridy = 6;
		gridBag.setConstraints(labelSellDate, gbc);
		contentPane.add(labelSellDate);
		
		// textFieldSellDate
		gbc.gridx = 2;
		gbc.gridy = 6;
		gridBag.setConstraints(textFieldSellDate, gbc);
		contentPane.add(textFieldSellDate);
		
		// labelDummySpace3
		gbc.gridx = 1;
		gbc.gridy = 7;
		gbc.gridwidth = 3;
		gridBag.setConstraints(labelDummySpace3, gbc);
		contentPane.add(labelDummySpace3);

		gbc.gridwidth = 1;
		
		/*
		 * Buttons
		 */
		// buttonCurrentDate
		gbc.gridx = 3;
		gbc.gridy = 6;
		gridBag.setConstraints(buttonCurrentDate, gbc);
		contentPane.add(buttonCurrentDate);
		
		// buttonSave
		gbc.gridx = 1;
		gbc.gridy = 8;
		gbc.fill = GridBagConstraints.FIRST_LINE_START;
		gbc.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(buttonSell, gbc);
		contentPane.add(buttonSell);
		
		// buttonCancel
		gbc.gridx = 2;
		gbc.gridy = 8;
		gbc.fill = GridBagConstraints.FIRST_LINE_END;
		gbc.anchor = GridBagConstraints.EAST;
		gridBag.setConstraints(buttonCancel, gbc);
		contentPane.add(buttonCancel);
		
		/*
		 * Actions
		 */
		// buttonCurrentDate
		buttonCurrentDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				// Setzen des aktuellen Datums mit Uhrzeit ins BuyDate Textfeld
				textFieldSellDate.setText(Stock.getCurrentDate());
			}
		});
		
		// buttonSell
		buttonSell.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				// Leere Textfelder abfangen
				if (textFieldSellRate.getText().isEmpty())
					MessageDialogs.createErrorDialogEmptyFields("Verkaufs-Preis");
				
				if (textFieldSellDate.getText().isEmpty())
					MessageDialogs.createErrorDialogEmptyFields("Verkaufs-Datum");
				
				if (!textFieldSellRate.getText().isEmpty() && !textFieldSellDate.getText().isEmpty()) {

					// Debug
					System.out.println("changed Item: " + selectedItem);
					
					// Verkaufen der ausgewaehlten Aktie
					list.get(selectedItem).setSold(true);
					list.get(selectedItem).setSellRate(Double.parseDouble(textFieldSellRate.getText()));
					list.get(selectedItem).setSellDate(textFieldSellDate.getText());
					
					// Aktualisieren der Tabelle
					AllStocks.fireTableDataChanged();

					// HashMap aktualisieren
					Chart.updateStockValueHistory(selectedItem);
					Chart.updateDepotValueHistory();
					
					// Debug
					System.out.println("es wurde eine vorhandene Aktie verkauft...");
					
					setVisible(false);
					dispose();
				}
			}
		});
		
		// buttonCancel
		buttonCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				//System.exit(0);
				setVisible(false);
				dispose();
			}
		});
		
		return contentPane;
	}
	
	public SellStock(String title, int x, int y, int selectedRow) {
		if (list.get(selectedRow).isSold()) {
			MessageDialogs.createMessageDialog("Aktie verkaufen", "Die Aktie ist bereits verkauft.");
		} else {
			this.setTitle(title);
			this.setSize(x, y);
			// zentrale Positionierung des Fensters
			this.setLocationRelativeTo(null);
			
			SellStockContainer(selectedRow);
			
			this.setVisible(true);
		}
	}

	/* Methode, die beim Inhaltswechsel der ComboBox automatisch den Index in die
	 * Variable selectedItem schreibt.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JComboBox cb = (JComboBox)e.getSource();
		int selectedItem = cb.getSelectedIndex();
		
		// Debug
		System.out.println("comboBox, selectedItem: " + selectedItem);
		
		this.selectedItem = selectedItem;
		
		setTextFields();
	}
	
	/*
	 * Methode zum Setzen der Textfelder beim Wechsel der Combobox
	 */
	public void setTextFields() {
		
		// Debug
		System.out.println("setTextFields, selectedItem: " + selectedItem);
		
		/* 
		 * Fuer Aktien, die noch nicht verkauft worden sind, werden in die Textfelder
		 * leere Strings eingefuegt.
		 */
		if (list.get(selectedItem).isSold()) {
			Double getSellRate = list.get(selectedItem).getSellRate();
			textFieldSellRate.setText(getSellRate.toString());
			
			textFieldSellDate.setText(list.get(selectedItem).getSellDate()); 
		} else {
			textFieldSellRate.setText("");
			textFieldSellDate.setText("");
		}
	}
}
