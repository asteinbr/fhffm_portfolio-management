/**
 * 
 */
package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import model.Calculations;
import model.StocksModel;
import model.Stock;

/**
 * @author alexander
 * 
 * Class - DeleteStock
 * 
 * Diese Klasse definiert den "Aktien löschen"-Dialog.
 * Nutzung des Grid-Layouts.
 * Nutzung der vorhandenen Statistik-Methoden.
 */
public class Statistics extends JFrame {
	public StocksModel instance = StocksModel.getInstance();
	public ArrayList<Stock> list = StocksModel.getInstance().getStocksList();
	
	/*
	 * Methode erzeugt das Gesamt-Statistiken Panel und gibt dieses als Component zurück
	 */
	private JPanel createTotalsPanel() {
		/*
		 * Gesamt
		 */
		JPanel totals = new JPanel();
		totals.setBorder(BorderFactory.createTitledBorder("Gesamt"));
		totals.setSize(50, 50);
		
		// Aktien
		JLabel labelQuantity = new JLabel("Aktien: ");
		JLabel labelQuantityContent = new JLabel();
			labelQuantityContent.setText(Integer.toString(list.size()));
			labelQuantityContent.setToolTipText("Anzahl der verschiedenen Unternehmen in der Aktien Liste");
			
		// Summe
		JLabel labelVolume = new JLabel("Summe Aktien: ");
		JLabel labelVolumeContent = new JLabel();
			labelVolumeContent.setText(Integer.toString(Calculations.getStockSum()));
			labelVolumeContent.setToolTipText("Summe aller Aktien");
		
		// Layout erstellen
		GridLayout totalsGridLayout = new GridLayout(3,2);
		totals.setLayout(totalsGridLayout);
		
		// Elemente ins Layout hinzufügen
		totals.add(labelQuantity);
		totals.add(labelQuantityContent);
		
		totals.add(labelVolume);
		totals.add(labelVolumeContent);
		
		return totals;
	}
	
	private JPanel createAssetsPanel() {
		/*
		 * Assets
		 */
		JPanel assets = new JPanel();
		assets.setBorder(BorderFactory.createTitledBorder("Investmentkapital"));
		assets.setSize(50, 50);

		// Depotwert
		JLabel labelDepotValue = new JLabel("Depotwert: ");
			labelDepotValue.setToolTipText("Gesamtwert der Aktien im Depot");
		JLabel labelDepotValueContent = new JLabel();
			labelDepotValueContent.setText(Calculations.getFormat(Calculations.getCurrentDepotValue()) + " €");
			labelDepotValueContent.setToolTipText("aktuelles Kapital aller Aktien (aktueller Preis)");
		
		// Kaufwert
		JLabel labelBuyDepotValue = new JLabel("Kaufwert: ");
			labelBuyDepotValue.setToolTipText("Kaufwert der Aktien im Depot");
		JLabel labelBuyDepotValueContent = new JLabel();
			labelBuyDepotValueContent.setText(Calculations.getFormat(Calculations.getBuyDepotValue()) + " €");
			labelBuyDepotValueContent.setToolTipText("gesamtes Kapital wofür Aktien gekauft wurden");
		
		// Difference
		JLabel labelDifference = new JLabel("Differenz: ");
		JLabel labelDifferenceContent = new JLabel();
		
		// farbliche Hervorhebung bei Gewinn oder Verlust
		if (Calculations.getDepotChange() > 0.0) {
			labelDifferenceContent.setText(Calculations.getFormat(Calculations.getDepotChange()) + " €");
			labelDifferenceContent.setForeground(Color.GREEN);
		} else if (Calculations.getDepotChange() == 0.0) {
			labelDifferenceContent.setText(Calculations.getFormat(Calculations.getDepotChange()) + " €");
		} else {
			labelDifferenceContent.setText(Calculations.getFormat(Calculations.getDepotChange()) + " €");
			labelDifferenceContent.setForeground(Color.RED);
		}
		labelDifferenceContent.setToolTipText("Differenz zum Kaufpreis");
		
		JLabel labelDummySpace4 = new JLabel(" ");
		JLabel labelDifferencePercentContent = new JLabel();
		
		// farbliche Hervorhebung bei Gewinn oder Verlust
		if (Calculations.getPerformanceAssetPercent() > 0.0) {
			labelDifferencePercentContent.setText(Calculations.getFormat(Calculations.getPerformanceAssetPercent()) + " %");
			labelDifferencePercentContent.setForeground(Color.GREEN);
		} else if (Calculations.getPerformanceAssetPercent() == 0.0) {
			labelDifferencePercentContent.setText(Calculations.getFormat(Calculations.getPerformanceAssetPercent()) + " %");
		} else {
			labelDifferencePercentContent.setText(Calculations.getFormat(Calculations.getPerformanceAssetPercent()) + " %");
			labelDifferencePercentContent.setForeground(Color.RED);
		}
		labelDifferencePercentContent.setToolTipText("Differenz zum Kaufpreis");
		
		// Layout erstellen
		GridLayout assetsGridLayout = new GridLayout(5,2);
		assets.setLayout(assetsGridLayout);
		
		// Elemente ins Layout hinzufügen
		assets.add(labelDepotValue);
		assets.add(labelDepotValueContent);
		
		assets.add(labelBuyDepotValue);
		assets.add(labelBuyDepotValueContent);
		
		assets.add(labelDifference);
		assets.add(labelDifferenceContent);
		
		assets.add(labelDummySpace4);
		assets.add(labelDifferencePercentContent);
		
		return assets;
	}
	
	private JPanel createWinLossPanel() {
		JPanel winLoss = new JPanel();
		
		winLoss.setBorder(BorderFactory.createTitledBorder("GuV"));
		winLoss.setSize(50, 50);
		
		JLabel labelWinLossContent = new JLabel();
		//JLabel labelWinLossPercentContent = new JLabel();
		
		// farbliche Hervorhebung bei Gewinn oder Verlust
		if (Calculations.getCurrentWinLoss() > 0.0) {
			labelWinLossContent.setText(Calculations.getFormat(Calculations.getCurrentWinLoss()) + " €");
			labelWinLossContent.setForeground(Color.GREEN);
		} else if (Calculations.getCurrentWinLoss() == 0.0) {
			labelWinLossContent.setText(Calculations.getFormat(Calculations.getCurrentWinLoss()) + " €");
			labelWinLossContent.setForeground(Color.BLACK);
		} else {
			labelWinLossContent.setText(Calculations.getFormat(Calculations.getCurrentWinLoss()) + " €");
			labelWinLossContent.setForeground(Color.RED);
		}
		labelWinLossContent.setToolTipText("Gesamte Gewinn/Verlust aller verkauften Aktien");
		
		winLoss.add(labelWinLossContent);
		
		return winLoss;
	}
	
	private Component createTotalsStatisticsPanel() {
		JPanel all = new JPanel();
		
		all.setLayout(new BorderLayout());
		all.add(createTotalsPanel(), "North");
		all.add(createAssetsPanel(), "Center");
		all.add(createWinLossPanel(), "South");
		
		return all;
	}
	
	/*
	 * Methode erzeugt eine ButtonBar und gibt diese zurueck
	 */
	private JPanel createButtonBar() {
		JPanel panel;
		JButton buttonClose = new JButton("Schließen");
		
		FlowLayout layout = new FlowLayout();
		
		this.setLayout(layout);
		this.add(buttonClose);
		
		panel = new JPanel(layout);
		
		// Action "Save" (Fenster schließen)
		buttonClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				setVisible(false);
			}
		});
		
		return panel;
	}
	
	/*
	 * Konstruktor
	 */
	public Statistics(String title, int x, int y, int selectedRow) {
		
		this.setTitle(title);
		this.setSize(x, y);
		// zentrale Positionierung des Fensters
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		// Hinzufügen der Panels
		this.add(createTotalsStatisticsPanel(), BorderLayout.NORTH);
		this.add(createButtonBar(), BorderLayout.SOUTH);
	}
}
