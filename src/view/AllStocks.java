/**
 * 
 */
package view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import model.DataProvider;
import model.StocksModel;
import model.PortfolioManagement;
import model.Stock;
import model.TableModelStocks;


/**
 * @author alexander
 * 
 * Class - AllStocks
 * 
 * Diese Klasse definiert den Hauptdialog der Anwedung. Dier besteht aus der Menüleiste, Tabelle, Buttonbar.
 */
public class AllStocks extends JFrame implements TableModelListener {
	public StocksModel instance = StocksModel.getInstance();
	public ArrayList<Stock> list = StocksModel.getInstance().getStocksList();

	public static TableModelStocks tableModel = new TableModelStocks();
	public static JTable table = new JTable(tableModel);
	public int selectedRow = -1;
	public int[] selectedRows = null;
	
	// MenuItems
	private JMenu m_file = new JMenu("Datei");
		private JMenuItem mi_create = new JMenuItem("Neu");
		private JMenuItem mi_load = new JMenuItem("Laden");
		private JMenuItem mi_save = new JMenuItem("Speichern");
		private JMenu m_csv = new JMenu("CSV");
			private JMenuItem mi_csvimport = new JMenuItem("Import");
			private JMenuItem mi_csvexport = new JMenuItem("Export");
		private JMenuItem mi_close = new JMenuItem("Schließen");
	
	private JMenu m_tasks = new JMenu("Aufgaben");
		private JMenuItem mi_buyStock = new JMenuItem("Aktie kaufen");
		private JMenuItem mi_modifyStock = new JMenuItem("Aktie bearbeiten");
		private JMenuItem mi_sellStock = new JMenuItem("Aktie verkaufen");
		private JMenuItem mi_deleteStock = new JMenuItem("Aktie löschen");
		private JMenuItem mi_infoStock = new JMenuItem("Aktie Info");
		private JMenuItem mi_statistics = new JMenuItem("Statistiken");
		private JMenuItem mi_updateCurrentRate = new JMenuItem("akt. Preis aktualisieren");
		
	private JMenu m_charts = new JMenu("Charts");
		private JMenuItem mi_stockChart = new JMenuItem("Aktien Chart");
		private JMenuItem mi_depotValueChart = new JMenuItem("Depotkursverlauf");
		private JMenuItem mi_depotPieChart = new JMenuItem("Aktienverteilung");
		
	private JMenu m_question_mark = new JMenu("?");
		private JMenuItem mi_info = new JMenuItem("Info");
	
	// Buttons
	private JButton buttonBuyStock = new JButton("Aktie kaufen");
	private JButton buttonModifyStock = new JButton("Aktie verändern");
	private JButton buttonSellStock = new JButton("Aktie verkaufen");
	private JButton buttonDeleteStock = new JButton("Aktie löschen");
	private JButton buttonStatistics = new JButton("Statistiken");
	private JButton buttonInfoStock = new JButton("Info");
	
	/*
	 * Anpassung der Spalten-Breite an die notwendige Größe
	 */
	private void setTableResize() {
		// Tabelle an die Breite der Spalten anpassen
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		TableColumnModel columnModel = table.getColumnModel();

		columnModel.getColumn(0).setPreferredWidth(30);	//ID
		columnModel.getColumn(1).setPreferredWidth(65);	//WKN
		columnModel.getColumn(2).setPreferredWidth(185);//Name
		columnModel.getColumn(3).setPreferredWidth(50);	//Anzahl / #
		columnModel.getColumn(4).setPreferredWidth(60);	//Preis
		columnModel.getColumn(5).setPreferredWidth(125);//K-Datum
		columnModel.getColumn(6).setPreferredWidth(60);	//K-Preis
		columnModel.getColumn(7).setPreferredWidth(26);	//VK?
		columnModel.getColumn(8).setPreferredWidth(60);	//VK-Preis
		columnModel.getColumn(9).setPreferredWidth(135);//Performance
	}
	
	/*
	 * Aktivierung der Sortierung in der Tabelle
	 */
	private void setTableSort() {
		// Deklaration neuer TableRowSorter
		TableRowSorter<TableModel> tableSort = new TableRowSorter<TableModel>();
		
		// Bestimmen, welcher TableRowSorter eingesetzt werden muss
		table.setRowSorter(tableSort);
		
		// Bestimmen, welches Model sortiert wreden muss
		tableSort.setModel(tableModel);
	}
	
	/*
	 * 
	 */
	private void addMenuItemsToListener() {
		MenuListener ml = new MenuListener();
		
		mi_create.addMouseListener(ml);
		mi_load.addMouseListener(ml);
		mi_save.addMouseListener(ml);
		mi_csvimport.addMouseListener(ml);
		mi_csvexport.addMouseListener(ml);
		mi_close.addMouseListener(ml);
		
		mi_buyStock.addMouseListener(ml);
		mi_modifyStock.addMouseListener(ml);
		mi_sellStock.addMouseListener(ml);
		mi_deleteStock.addMouseListener(ml);
		mi_infoStock.addMouseListener(ml);
		mi_statistics.addMouseListener(ml);
		mi_updateCurrentRate.addMouseListener(ml);
		mi_info.addMouseListener(ml);
		
		mi_stockChart.addMouseListener(ml);
		mi_depotValueChart.addMouseListener(ml);
		mi_depotPieChart.addMouseListener(ml);
		
		buttonBuyStock.addMouseListener(ml);
		buttonModifyStock.addMouseListener(ml);
		buttonSellStock.addMouseListener(ml);
		buttonDeleteStock.addMouseListener(ml);
		buttonStatistics.addMouseListener(ml);
		buttonInfoStock.addMouseListener(ml);
	}
	
	/*
	 * Aktivierung der Tasten-Schnell-Zugriffe
	 */
	private void setMenuItemsMnemonics() {
		// Menubar File/Datei
		m_file.setMnemonic(KeyEvent.VK_D);
			mi_create.setMnemonic(KeyEvent.VK_N);
			mi_create.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
			mi_load.setMnemonic(KeyEvent.VK_L);
			mi_load.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK));
			mi_save.setMnemonic(KeyEvent.VK_S);
			mi_save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
			mi_csvimport.setMnemonic(KeyEvent.VK_I);
			mi_csvimport.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));
			mi_csvexport.setMnemonic(KeyEvent.VK_E);
			mi_csvexport.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
			mi_close.setMnemonic(KeyEvent.VK_Q);
			mi_close.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		
		// Menubar Task/Aufgaben
		m_tasks.setMnemonic(KeyEvent.VK_T);
			mi_buyStock.setMnemonic(KeyEvent.VK_1);
			mi_buyStock.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.CTRL_MASK));
			mi_modifyStock.setMnemonic(KeyEvent.VK_2);
			mi_modifyStock.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.CTRL_MASK));
			mi_sellStock.setMnemonic(KeyEvent.VK_3);
			mi_sellStock.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, ActionEvent.CTRL_MASK));
			mi_deleteStock.setMnemonic(KeyEvent.VK_4);
			mi_deleteStock.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.CTRL_MASK));
			mi_infoStock.setMnemonic(KeyEvent.VK_5);
			mi_infoStock.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.CTRL_MASK));
		
			mi_statistics.setMnemonic(KeyEvent.VK_6);
			mi_statistics.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6, ActionEvent.CTRL_MASK));

			mi_updateCurrentRate.setMnemonic(KeyEvent.VK_7);
			mi_updateCurrentRate.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_7, ActionEvent.CTRL_MASK));
			
		// Menubar ?
		m_question_mark.setMnemonic(KeyEvent.VK_H);
			mi_info.setMnemonic(KeyEvent.VK_V);
			mi_info.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
	}
	
	/*
	 * Erstellen der Menübar
	 */
	private void createMenuBar() {
		// Menubar erzeugen
		JMenuBar menuBar = new JMenuBar();
		
		JSeparator sep_file = new JSeparator();
		JSeparator sep_tasks = new JSeparator();
		
		// MenuBar integrieren
		menuBar.add(m_file);
			m_file.add(mi_create);
			m_file.add(mi_load);
			m_file.add(mi_save);
			m_file.add(m_csv);
				m_csv.add(mi_csvimport);
				m_csv.add(mi_csvexport);
			m_file.add(sep_file);
			m_file.add(mi_close);
		menuBar.add(m_tasks);
			m_tasks.add(mi_buyStock);
			m_tasks.add(mi_modifyStock);
			m_tasks.add(mi_sellStock);
			m_tasks.add(mi_deleteStock);
			m_tasks.add(mi_infoStock);
			m_tasks.add(sep_tasks);
			m_tasks.add(mi_statistics);
			m_tasks.add(mi_updateCurrentRate);
		menuBar.add(m_charts);
			m_charts.add(mi_stockChart);
			m_charts.add(mi_depotValueChart);
			m_charts.add(mi_depotPieChart);
		menuBar.add(m_question_mark);
			m_question_mark.add(mi_info);
		
		// Items zum MouseListener hinzufügen
		addMenuItemsToListener();
		// Mnemonics aktivieren
		//setMenuItemsMnemonics();
		
		this.setJMenuBar(menuBar);
	}
	
	/*
	 * Einbinden der Table ins Layout
	 */
	private void createTableView() {
		// Container Table
		Container contentPaneNorth = getContentPane();
		contentPaneNorth.add(new JScrollPane(table), "Center");
		table.getModel().addTableModelListener(this);
	}
	
	/*
	 * Erzeugung der Buttonbar
	 */
	private void createButtonBar() {
		// Container Buttons
		Container contentPaneSouth = getContentPane();
		
		JPanel panelButton = new JPanel();
		panelButton.add(buttonBuyStock);
		panelButton.add(buttonModifyStock);
		panelButton.add(buttonSellStock);
		panelButton.add(buttonDeleteStock);
		panelButton.add(buttonStatistics);
		panelButton.add(buttonInfoStock);
		contentPaneSouth.add(panelButton, "South");
	}
	
	/*
	 * Methode läd die aktuellen Preise aller Aktien aus der Liste und speichert diese in die Liste ab
	 */
	protected void updateCurrentRate() {
		for (int i = 0; i < list.size(); i++) {
			String wkn = list.get(i).getWkn();
			list.get(i).setCurRate(Double.parseDouble(DataProvider.getCurrentRateFromWKN(wkn)));
		}
	}

	/*
	 * Konstruktor der Klasse
	 */
	public AllStocks(int x, int y) {
		this.setTitle(PortfolioManagement.applicationName + " v" + 
				PortfolioManagement.applicationVersion);
		this.setSize(x, y);
		// zentrale Positionierung des Fensters
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// TableModelListener
		table.getModel().addTableModelListener(this);
		
		// Modifzieren und Einbinden
		setTableResize();
		setTableSort();
		createMenuBar();
		createTableView();
		createButtonBar();
		
		this.setVisible(true);
	}
	
	@Override
	public void tableChanged(TableModelEvent e) {
		int row = e.getFirstRow();
		int column = e.getColumn();
		TableModel model = (TableModel)e.getSource();
		String columnName = model.getColumnName(column);
		Object data = model.getValueAt(row, column);
	}
	
	/*
	 * Methode zur Aktualisierung der Tabelle
	 */
	public static void fireTableDataChanged() {
		tableModel.fireTableDataChanged();
		tableModel.fireTableStructureChanged();
		table.repaint();
		table.updateUI();
		System.out.println("Tabelle wurde aktualisiert...");
	}
	
	/*
	 * Methode gibt die ID des ausgewaehlter Zeile zurueck 
	 */
	public int getSelectedRow() {
		// Merken der ausgewählten Zeile
		table.setRowSelectionAllowed(true);
		if (table.getRowSelectionAllowed()) {
			selectedRow = table.getSelectedRow();
			System.out.println("selectedRow: " + selectedRow);
		}
		
		return selectedRow;
	}
	
	/*
	 * Speichert die ausgewählten Zeilen im Array ab 
	 */
	public int[] getSelectedRows() {
		// Merken der ausgewählten Zeile
		table.setRowSelectionAllowed(true);
		if (table.getRowSelectionAllowed()) {
			selectedRows = table.getSelectedRows();
			for (int i = 0; i < selectedRows.length; i++) {
				// convertRowIndexToModel ist umbedingt notwendig, um die Reihenfolge 
				// der ausgewählten ID nach einer Umsortierung nach Spalten beizubehalten
				selectedRows[i] = table.convertRowIndexToModel(selectedRows[i]);
				// Debug
				System.out.println("selectedRows: " + selectedRows[i]);
			}
		}
		
		return selectedRows;
	}
	
	/*
	 * Klasse um Mausklicks abzufangen
	 * 
	 * z.B. beim Klicken auf ein Menüpunkt
	 */
	class MenuListener extends MouseAdapter {
		public void mouseReleased(MouseEvent me) {

			// -------------------
			// Datei
			// -------------------
			
			// Neu
			if (me.getSource().equals(mi_create)) {
				// ArrayList leeren
				list.clear();
				
				// Counter auf 0 setzen
				Stock.resetCounter();
				
				// HashMap aktualisieren
				Stock.clearDepotValueHistory();
				Chart.updateDepotValueHistory();
				
				// Tabelle aktualisieren
				fireTableDataChanged();
			}
			
			// Laden
			if (me.getSource().equals(mi_load)) {
				// Initialisieren vom JFileChooser
				JFileChooser fc = new JFileChooser();
				
				// Dialogtitel
				fc.setDialogTitle("Laden - Wählen Sie eine Datei aus...");
				
				// State des Dialogs wird abgespeichert
				int state = fc.showOpenDialog(null);
				
				if(state == JFileChooser.APPROVE_OPTION) {
					// Aufruf der Funktion csvImport
					
					StocksModel.readSer(fc.getSelectedFile().toString());
					
					tableModel.fireTableDataChanged();
					fireTableDataChanged();
				// Fehlerdialog
				} else {
					MessageDialogs.createMessageDialog("Information", "Es wurde keine Datei ausgewählt.");
				}
				
				Chart.updateDepotValueHistory();
				
				fireTableDataChanged();
			}
			
			// Speichern
			if (me.getSource().equals(mi_save)) {
				// Initialisieren vom JFileChooser
				JFileChooser fc = new JFileChooser();
				
				// Dialogtitel
				fc.setDialogTitle("Speichern - Wählen Sie einen Ort aus...");
				
				// State des Dialogs wird abgespeichert
				int state = fc.showSaveDialog(null);
				
				if(state == JFileChooser.APPROVE_OPTION) {
					//File file = fc.getSelectedFile();
					
					StocksModel.writeSer(fc.getSelectedFile().toString());
					
				// Fehlerdialog
				} else {
					MessageDialogs.createMessageDialog("Information", "Es wurd keine Datei gespeichert.");
				}
			}
			
			// CSV-Import
			if (me.getSource().equals(mi_csvimport)) {
				// Initialisieren vom JFileChooser
				JFileChooser fc = new JFileChooser();
				
				// Dialogtitel
				fc.setDialogTitle("CSV-Import - Wählen Sie eine Datei aus...");
				
				// State des Dialogs wird abgespeichert
				int state = fc.showOpenDialog(null);
				
				if(state == JFileChooser.APPROVE_OPTION) {
					// Aufruf der Funktion csvImport
					instance.csvImportFull(fc.getSelectedFile().toString());
					tableModel.fireTableDataChanged();
					fireTableDataChanged();
				// Fehlerdialog
				} else {
					MessageDialogs.createMessageDialog("Information", "Es wurde keine Datei ausgewählt.");
				}
				
				Chart.updateDepotValueHistory();
				
				fireTableDataChanged();
			}
			
			// CSV-Export
			if (me.getSource().equals(mi_csvexport)) {
				// Initialisieren vom JFileChooser
				JFileChooser fc = new JFileChooser();
				
				// Dialogtitel
				fc.setDialogTitle("CSV-Export - Wählen Sie einen Ort aus...");
				
				// State des Dialogs wird abgespeichert
				int state = fc.showSaveDialog(null);
				
				if(state == JFileChooser.APPROVE_OPTION) {
					//File file = fc.getSelectedFile();
					instance.csvExportFull(fc.getSelectedFile().toString());
				// Fehlerdialog
				} else {
					MessageDialogs.createMessageDialog("Information", "Es wurd keine Datei gespeichert.");
				}
			}

			// Schließen
			if (me.getSource().equals(mi_close)) {
				// Das Programm ohne Fehlermeldung beenden
				System.exit(0);
			}
			
			// -------------------
			// Tasks
			// -------------------
			
			// BuyStock-Dialog
			if (me.getSource().equals(mi_buyStock) || me.getSource().equals(buttonBuyStock)) {
				BuyStock buyStockDialog = new BuyStock("Aktie kaufen", 320, 270);
				System.out.println("BuyStock-Dialog via ButtonBar started...");
				
				// Tabelle aktualisieren
				fireTableDataChanged();
			}
			
			// ModifyStock-Dialog
			if (me.getSource().equals(mi_modifyStock) || me.getSource().equals(buttonModifyStock)) {
				if (list.isEmpty()) {
					MessageDialogs.createErrorDialog("Aktie bearbeiten - Fehler", "Ihre Tabelle enthält keine Daten.\nBitte fügen Sie erst welche ein und wiederholen Sie die Aktion.");
				} else {
					// Aktualisieren der selectedRow-Variable  
					getSelectedRow();
					
					// Aufruf des ModifyStock-Dialogs
					ModifyStock modifyStockDialog = new ModifyStock("Aktie bearbeiten", 320, 400, selectedRow);
					System.out.println("ModifyStock-Dialog via ButtonBar started...");
					
					// Tabelle aktualisieren
					fireTableDataChanged();
				}
			}
			
			// SellStock-Dialog
			if (me.getSource().equals(mi_sellStock) || me.getSource().equals(buttonSellStock)) {
				// Prüfen ob Daten vorhanden sind
				if (list.isEmpty()) {
					MessageDialogs.createErrorDialog("Aktie verkaufen - Fehler", "Ihre Tabelle enthält keine Daten.\nBitte fügen Sie erst welche ein und wiederholen Sie die Aktion.");
				} else {
					// Aktualisieren der selectedRow-Variable  
					getSelectedRow();
					
					if (selectedRow < 0)
						MessageDialogs.createErrorDialog("Aktie verkaufen - Fehler", "Es wurde kein Tabellensatz ausgewählt.");
					else {					
						// Aufruf des SellStock-Dialogs
						SellStock sellStockDialog = new SellStock("Aktie verkaufen", 320, 270, selectedRow);
						System.out.println("SellStock-Dialog via ButtonBar started...");

						// Tabelle aktualisieren
						fireTableDataChanged();
					}
				}
			}
			
			// DeleteStock-Dialog
			if (me.getSource().equals(mi_deleteStock) || me.getSource().equals(buttonDeleteStock)) {
				if (list.isEmpty()) {
					MessageDialogs.createErrorDialog("Aktie löschen - Fehler", "Ihre Tabelle enthält keine Daten.\nBitte fügen Sie erst welche ein und wiederholen Sie die Aktion.");
				} else {
					// Aktualisieren der selectedRow-Variable 
					getSelectedRow();
					
					// Aufruf des DeleteStock-Dialogs
					DeleteStock deleteStockDialog = new DeleteStock("Aktie löschen", 280, 200, selectedRow);
					System.out.println("DeleteStock-Dialog started...");
					
					// Tabelle aktualisieren
					fireTableDataChanged();
				}
			}
			
			// InfoStock-Dialog
			if (me.getSource().equals(mi_infoStock) || me.getSource().equals(buttonInfoStock)) {
				if (list.isEmpty()) {
					MessageDialogs.createErrorDialog("Aktie Info - Fehler", "Ihre Tabelle enthält keine Daten.\nBitte fügen Sie erst welche ein und wiederholen Sie die Aktion.");
				} else {
					// Aktualisieren der selectedRow-Variable 
					getSelectedRow();
					
					// Aufruf des InfoStock-Dialogs
					InfoStock infoStock = new InfoStock("Aktie Info", 550, 290, selectedRow);
					System.out.println("InfoStock-Dialog started...");
					
					// Tabelle aktualisieren
					fireTableDataChanged();
				}
			}
			
			// Statistics-Dialog
			if (me.getSource().equals(mi_statistics) || me.getSource().equals(buttonStatistics)) {
				// Aktualisieren der selectedRow-Variable
				getSelectedRow();
				
				// TODO *NOT COMPLETE YET*
				//getSelectedRows();				
				
				// Aufruf des Statistik-Dialogs
				Statistics statisticsDialog = new Statistics("Statistiken", 280, 290, selectedRow);
				System.out.println("Statistics-Dialog started...");
				
				// Tabelle aktualisieren
				fireTableDataChanged();
			}
			
			// Preis aktualisieren
			if (me.getSource().equals(mi_updateCurrentRate)) {
				if (list.isEmpty()) {
					MessageDialogs.createErrorDialog("Aktie Info - Fehler", "Ihre Tabelle enthält keine Daten.\nBitte fügen Sie erst welche ein und wiederholen Sie die Aktion.");
				} else {
					// Info-Meldung
					MessageDialogs.createMessageDialog("Information", "Es wird ein Update ausgeführt.");

					// Aufruf der Methode zum Aktualisieren der Preise
					updateCurrentRate();

					// HashMap aktualisieren
					for (int i = 0; i < list.size(); i++)
						Chart.updateStockValueHistory(i);
					Chart.updateDepotValueHistory();
					
					// Tabelle aktualisieren
					fireTableDataChanged();
				}
			}
			
			// -------------------
			// Charts
			// -------------------
			
			// Aktien Chart anzeigen
			if (me.getSource().equals(mi_stockChart)) {
				if (list.isEmpty()) {
					MessageDialogs.createErrorDialog("Aktie Chart - Fehler", "Ihre Tabelle enthält keine Daten.\nBitte fügen Sie erst welche ein und wiederholen Sie die Aktion.");
				} 
				
				// Aktualisieren der selectedRow-Variable
				getSelectedRow();
				
				if (selectedRow < 0)
					MessageDialogs.createErrorDialog("Aktie Chart - Fehler", "Sie müssen eine Aktie aus der Tabele auswählen!");
				else {
					// Aufruf des StockChart-Dialogs
					new Chart(1, selectedRow);
					System.out.println("Stock-Chart-Dialog for Stock-Id " + selectedRow + " started...");
				}
			}
			
			// Depotwert-Verlauf anzeigen
			if (me.getSource().equals(mi_depotValueChart)) {
				if (list.isEmpty()) {
					MessageDialogs.createErrorDialog("Aktie Chart - Fehler", "Ihre Tabelle enthält keine Daten.\nBitte fügen Sie erst welche ein und wiederholen Sie die Aktion.");
				} 
				
				// Aufruf des Depotwert-Verlauf-Dialogs
				new Chart(2, "Depotwert Verlauf");
				System.out.println("Share-Allocation-Dialog started...");
			}
			
			// Aktienverteilung im Depot anzeigen
			if (me.getSource().equals(mi_depotPieChart)) {
				if (list.isEmpty()) {
					MessageDialogs.createErrorDialog("Aktienverteilungs-Chart - Fehler", "Ihre Tabelle enthält keine Daten.\nBitte fügen Sie erst welche ein und wiederholen Sie die Aktion.");
				} 
				
				// Aufruf des Statistik-Dialogs
				new Chart(3, "Aktienverteilung im Depot");
				System.out.println("Share-Allocation-Dialog started...");
			}
			
			// -------------------
			// ?
			// -------------------
			
			// Info-Dialog
			if (me.getSource().equals(mi_info)) {
				Info info = new Info("Info", 250, 200);
				
				fireTableDataChanged();
				
				System.out.println("InfoStock-Dialog started...");
			}
		}
	}
	
	public static void setTableModel(TableModelStocks newTableModel) {
		tableModel = newTableModel;
	}
	
	public static TableModelStocks getTableModel() {
		return tableModel;
	}
}
