/**
 * 
 */
package view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import model.Calculations;
import model.DataProvider;
import model.StocksModel;
import model.Stock;


/**
 * @author alexander
 * 
 * Class - BuyStock
 * 
 * Diese Klasse definiert den "Aktien kaufen"-Dialog.
 * Nutzung des GridBag-Layouts.
 * Abfangen von Leer-Eingaben.
 */
public class BuyStock extends JFrame {
	public StocksModel instance = StocksModel.getInstance();
	public ArrayList<Stock> list = StocksModel.getInstance().getStocksList();
	
	private Container BuyStockContainer() {
		// GridBagLayout
		Container contentPane = getContentPane();
		GridBagLayout gridBag = new GridBagLayout();
		contentPane.setLayout(gridBag);
		
		// Deklaration der Labels
		JLabel labelInfo = new JLabel("Neue Aktie eintragen:");
		JLabel labelId = new JLabel("ID:");
		JLabel labelWkn = new JLabel("WKN:");
		JLabel labelName = new JLabel("Name:");
		JLabel labelQuantity = new JLabel("Anzahl:");
		JLabel labelBuyRate = new JLabel("Kauf-Kurs:");
		JLabel labelBuyDate = new JLabel("Kauf-Datum:");
		JLabel labelDummySpace1 = new JLabel(" ");
		JLabel labelDummySpace2 = new JLabel(" ");
		
		// Deklaration der Textfelder
		final JTextField textFieldId = new JTextField(7);
			textFieldId.setEditable(false);
			// naechste ID ins ID-Feld hinzufuegen
			/*Integer nextId = list.get(0).getNextId();*/
			textFieldId.setText(Integer.toString(Stock.getNextId()));
			
		final JTextField textFieldWkn = new JTextField(7);
		final JTextField textFieldName = new JTextField(14);
		final JTextField textFieldQuantity = new JTextField(7);
		final JTextField textFieldBuyRate = new JTextField(7);
		final JTextField textFieldBuyDate = new JTextField(14);
		
		// Deklaration der Buttons
		JButton buttonGetName = new JButton("*");
			buttonGetName.setToolTipText("Namen für eingegebene WKN einfügen (Online-Query)");
			buttonGetName.setPreferredSize(new Dimension(18, 18));
			
		JButton buttonCurrentDate = new JButton("*");
			buttonCurrentDate.setToolTipText("aktuelles Datum einfügen");
			buttonCurrentDate.setPreferredSize(new Dimension(18, 18));
			
		JButton buttonSave = new JButton("Speichern");
		JButton buttonCancel = new JButton("Abbrechen");
		
		{
			/*
			 * Hinzufügen der einzelnen Elemente
			 * 
			 * Jedes Element braucht einen GridBagConstraint mit Details über
			 * die Positionierung
			 */
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(2, 2, 2, 2);
			gbc.anchor = GridBagConstraints.LINE_START;
			gbc.fill = GridBagConstraints.FIRST_LINE_START;
			gbc.anchor = GridBagConstraints.WEST;
			
			// labelInfo
			gbc.gridx = 1;
			gbc.gridy = 1;
			gbc.gridwidth = 2;
			gridBag.setConstraints(labelInfo, gbc);
			contentPane.add(labelInfo);

			// labelDummySpace
			gbc.gridx = 1;
			gbc.gridy = 2;
			gbc.gridwidth = 3;
			gridBag.setConstraints(labelDummySpace1, gbc);
			contentPane.add(labelDummySpace1);
			
			gbc.gridwidth = 1;
			
			// labelId
			gbc.gridx = 1;
			gbc.gridy = 3;
			gridBag.setConstraints(labelId, gbc);
			contentPane.add(labelId);
			
			// textFieldId
			gbc.gridx = 2;
			gbc.gridy = 3;
			gridBag.setConstraints(textFieldId, gbc);
			contentPane.add(textFieldId);
			
			// labelWkn
			gbc.gridx = 1;
			gbc.gridy = 4;
			gridBag.setConstraints(labelWkn, gbc);
			contentPane.add(labelWkn);

			// textFieldWkn
			gbc.gridx = 2;
			gbc.gridy = 4;
			gridBag.setConstraints(textFieldWkn, gbc);
			contentPane.add(textFieldWkn);
			
			// labelName
			gbc.gridx = 1;
			gbc.gridy = 5;
			gridBag.setConstraints(labelName, gbc);
			contentPane.add(labelName);

			// textFieldName
			gbc.gridx = 2;
			gbc.gridy = 5;
			gridBag.setConstraints(textFieldName, gbc);
			contentPane.add(textFieldName);
			
			// labelQuantity
			gbc.gridx = 1;
			gbc.gridy = 6;
			gridBag.setConstraints(labelQuantity, gbc);
			contentPane.add(labelQuantity);

			// textFieldQuantity
			gbc.gridx = 2;
			gbc.gridy = 6;
			gridBag.setConstraints(textFieldQuantity, gbc);
			contentPane.add(textFieldQuantity);
			
			// labelBuyRate
			gbc.gridx = 1;
			gbc.gridy = 7;
			gridBag.setConstraints(labelBuyRate, gbc);
			contentPane.add(labelBuyRate);

			// textFieldBuyRate
			gbc.gridx = 2;
			gbc.gridy = 7;
			gridBag.setConstraints(textFieldBuyRate, gbc);
			contentPane.add(textFieldBuyRate);
			
			// labelBuyDate
			gbc.gridx = 1;
			gbc.gridy = 8;
			gridBag.setConstraints(labelBuyDate, gbc);
			contentPane.add(labelBuyDate);

			// textFieldBuyDate
			gbc.gridx = 2;
			gbc.gridy = 8;
			gridBag.setConstraints(textFieldBuyDate, gbc);
			contentPane.add(textFieldBuyDate);
			
			// labelDummySpace
			gbc.gridx = 1;
			gbc.gridy = 9;
			gbc.gridwidth = 2;
			gridBag.setConstraints(labelDummySpace2, gbc);
			contentPane.add(labelDummySpace2);
			
			gbc.gridwidth = 1;
			
			/*
			 * Buttons
			 */
			// buttonGetName
			gbc.gridx = 3;
			gbc.gridy = 5;
			gridBag.setConstraints(buttonGetName, gbc);
			contentPane.add(buttonGetName);
			
			// buttonCurrentDate
			gbc.gridx = 3;
			gbc.gridy = 8;
			gridBag.setConstraints(buttonCurrentDate, gbc);
			contentPane.add(buttonCurrentDate);
			
			// buttonSave
			gbc.gridx = 1;
			gbc.gridy = 10;
			gbc.gridwidth = 1;
			gbc.fill = GridBagConstraints.FIRST_LINE_START;
			gbc.anchor = GridBagConstraints.WEST;
			gridBag.setConstraints(buttonSave, gbc);
			contentPane.add(buttonSave);

			// buttonCancel
			gbc.gridx = 2;
			gbc.gridy = 10;
			gbc.fill = GridBagConstraints.FIRST_LINE_END;
			gbc.anchor = GridBagConstraints.EAST;
			gridBag.setConstraints(buttonCancel, gbc);
			contentPane.add(buttonCancel);
		}
		
		/*
		 * Button Events
		 */
		
		// buttonGetName
		buttonGetName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				// Setzen des aktuellen Datums mit Uhrzeit ins BuyDate Textfeld
				textFieldName.setText(DataProvider.getNameFromWKN(textFieldWkn.getText()));
			}
		});
		
		// buttonCurrentDate
		buttonCurrentDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				// Setzen des aktuellen Datums mit Uhrzeit ins BuyDate Textfeld
				textFieldBuyDate.setText(Stock.getCurrentDate());
			}
		});
		
		// buttonSave
		buttonSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				// WKN muss 6 Zeichen beinhalten
				if (textFieldWkn.getText().length() != 6)
					MessageDialogs.createErrorDialog("Fehler", "Das Feld WKN muss 6 Zeichen beinhalten.");
				
				// Leere Textfelder abfangen
				if (textFieldWkn.getText().isEmpty())
					MessageDialogs.createErrorDialogEmptyFields("WKN");
				
				if (textFieldName.getText().isEmpty())
					MessageDialogs.createErrorDialogEmptyFields("Name");
				
				if (textFieldQuantity.getText().isEmpty())
					MessageDialogs.createErrorDialogEmptyFields("Anzahl");
				
				if (textFieldBuyRate.getText().isEmpty())
					MessageDialogs.createErrorDialogEmptyFields("Kauf-Preis");
				
				if (textFieldBuyDate.getText().isEmpty())
					MessageDialogs.createErrorDialogEmptyFields("Kauf-Datum");
				
				if (!textFieldWkn.getText().isEmpty() && 
						!textFieldName.getText().isEmpty() &&
						!textFieldQuantity.getText().isEmpty() && 
						!textFieldBuyRate.getText().isEmpty() && 
						!textFieldBuyDate.getText().isEmpty()) {
					// neues Stock-Objekt mit den eingegebenen Werten erzeugen
					list.add(new Stock(	textFieldWkn.getText().toString(), 
							textFieldName.getText().toString(),
							Integer.parseInt(textFieldQuantity.getText()), 
							Double.parseDouble(DataProvider.getCurrentRateFromWKN(textFieldWkn.getText())), 
							Double.parseDouble(textFieldBuyRate.getText()), 
							textFieldBuyDate.getText()));
					
					// Aktualisieren der Tabelle
					AllStocks.fireTableDataChanged();
					
					// BuyRate/BuyDate der Aktie der StockHistory hinzufügen
					list.get(list.size()-1).insertStockHistory(list.get((list.size()-1)).getBuyDate(), list.get((list.size()-1)).getBuyRate());
					// Depotwert + Einkaufsdaten der Aktie der Depothistory hinzufügen
					Stock.insertDepotHistory(list.get((list.size()-1)).getBuyDate(), Calculations.getBuyValue(list.size()-1) + 
							(Calculations.getCurrentDepotValue() - Calculations.getCurrentValue(list.size()-1)));
					
					// TODO *Debug-Output*
					System.out.println("es wurde eine neue Aktie angelegt...");
					
					setVisible(false);
					dispose();
				}
			}
		});
		
		// buttonCancel
		buttonCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				//System.exit(0);
				setVisible(false);
				dispose();
			}
		});
		
		return contentPane;
	}
	
	public BuyStock(String title, int x, int y) {
		this.setTitle(title);
		this.setSize(x, y);
		// zentrale Positionierung des Fensters
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		// Container mit Inhalt einbinden
		BuyStockContainer();
	}
}

