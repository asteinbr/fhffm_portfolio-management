/**
 * 
 */
package view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import model.DataProvider;
import model.StocksModel;
import model.Stock;

/**
 * @author alexander
 * 
 * Class - ModifyStock
 * 
 * Diese Klasse definiert den "Aktien bearbeiten"-Dialog.
 * Nutzung des GridBag-Layouts.
 * Abfangen von Leer-Eingaben.
 */
public class ModifyStock extends JFrame implements ActionListener {
	public StocksModel instance = StocksModel.getInstance();
	public ArrayList<Stock> list = StocksModel.getInstance().getStocksList();
	private int selectedItem = 0;
	
	// Deklaration der Textfelder
	private JTextField textFieldId = new JTextField(7);
	private JTextField textFieldWkn = new JTextField(7);
	private JTextField textFieldName = new JTextField(14);
	private JTextField textFieldQuantity = new JTextField(7);
	private JTextField textFieldCurrentRate = new JTextField(7);
	private JTextField textFieldBuyRate = new JTextField(7);
	private JTextField textFieldBuyDate = new JTextField(14);
	private JTextField textFieldSellRate = new JTextField(7);
	private JTextField textFieldSellDate = new JTextField(14);

	// Deklaration der CheckBox
	private JCheckBox checkBoxSold = new JCheckBox();
	
	private Container ModifyStockContainer(int selectedRow) {
		// GridBagLayout
		Container contentPane = getContentPane();
		GridBagLayout gridBag = new GridBagLayout();
		contentPane.setLayout(gridBag);
		
		// Textfeld-ID darf nicht editiert werden
		textFieldId.setEditable(false);
		
		// Deklaration der Labels
		JLabel labelInfo = new JLabel("Zum Editieren Aktie auswählen:");
		JLabel labelId = new JLabel("ID:");
		JLabel labelWkn = new JLabel("WKN:");
		JLabel labelName = new JLabel("Name:");
		JLabel labelQuantity = new JLabel("Anzahl:");
		JLabel labelCurrentRate = new JLabel("aktueller Preis:");
		JLabel labelBuyRate = new JLabel("Kauf-Kurs:");
		JLabel labelBuyDate = new JLabel("Kauf-Datum:");
		JLabel labelSold = new JLabel("Verkauft:");
		JLabel labelSellRate = new JLabel("Verkaufs-Kurs:");
		JLabel labelSellDate = new JLabel("Verkaufs-Datum:");
		JLabel labelDummySpace1 = new JLabel(" ");
		JLabel labelDummySpace2 = new JLabel(" ");
		JLabel labelDummySpace3 = new JLabel(" ");
		
		// Deklaration der comboBox
		JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(this);
		
		// Elemente aus der Aktien-Liste der comboBox hinzufügen
		for (int i = 0; i < list.size(); i++)
			comboBox.addItem(list.get(i).getName());
		
		// ComboBox auf das uebergebene Element setzen
		if (selectedRow >= 0)
			comboBox.setSelectedIndex(selectedRow);
		System.out.println("selectedRow: " + selectedRow);
		
		// Deklaration der Buttons
		JButton buttonGetName = new JButton();
			buttonGetName.setText(".");
			buttonGetName.setToolTipText("Namen für eingegebene WKN einfügen (Online-Query)");
			buttonGetName.setPreferredSize(new Dimension(18, 18));
		
		JButton buttonGetCurrentRate = new JButton();
			buttonGetCurrentRate.setText(".");
			buttonGetCurrentRate.setToolTipText("aktuellen Preis für eingegebene WKN einfügen (Online-Query)");
			buttonGetCurrentRate.setPreferredSize(new Dimension(18, 18));
		
		JButton buttonCurrentBuyDate = new JButton();
			buttonCurrentBuyDate.setText("*");
			buttonCurrentBuyDate.setToolTipText("aktuelles Datum einfügen");
			buttonCurrentBuyDate.setPreferredSize(new Dimension(18, 18));
		
		JButton buttonCurrentSellDate = new JButton();
			buttonCurrentSellDate.setText("*");
			buttonCurrentSellDate.setToolTipText("aktuelles Datum einfügen");
			buttonCurrentSellDate.setPreferredSize(new Dimension(18, 18));
		
		JButton buttonSave = new JButton("Speichern");
		JButton buttonCancel = new JButton("Abbrechen");
		
		/*
		 * Hinzufügen der einzelnen Elemente
		 * 
		 * Jedes Element braucht einen GridBagConstraint mit Details über
		 * die Positionierung
		 */
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(2, 2, 2, 2);
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.fill = GridBagConstraints.FIRST_LINE_START;
		gbc.anchor = GridBagConstraints.WEST;
		
		// labelInfo
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridwidth = 3;
		gridBag.setConstraints(labelInfo, gbc);
		contentPane.add(labelInfo);

		// labelDummySpace
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridwidth = 3;
		gridBag.setConstraints(labelDummySpace1, gbc);
		contentPane.add(labelDummySpace1);
		
		// comboBox
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.gridwidth = 4;
		gbc.fill = GridBagConstraints.BOTH;
		gridBag.setConstraints(comboBox, gbc);
		contentPane.add(comboBox);
		
		gbc.fill = GridBagConstraints.FIRST_LINE_START;
		
		// labelDummySpace2
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.gridwidth = 3;
		gridBag.setConstraints(labelDummySpace2, gbc);
		contentPane.add(labelDummySpace2);
		
		gbc.gridwidth = 1;
		
		// labelId
		gbc.gridx = 1;
		gbc.gridy = 5;
		gridBag.setConstraints(labelId, gbc);
		contentPane.add(labelId);
		
		// textFieldId
		gbc.gridx = 2;
		gbc.gridy = 5;
		gridBag.setConstraints(textFieldId, gbc);
		contentPane.add(textFieldId);
		
		// labelWkn
		gbc.gridx = 1;
		gbc.gridy = 6;
		gridBag.setConstraints(labelWkn, gbc);
		contentPane.add(labelWkn);

		// textFieldWkn
		gbc.gridx = 2;
		gbc.gridy = 6;
		gridBag.setConstraints(textFieldWkn, gbc);
		contentPane.add(textFieldWkn);
		
		// labelName
		gbc.gridx = 1;
		gbc.gridy = 7;
		gridBag.setConstraints(labelName, gbc);
		contentPane.add(labelName);

		// textFieldName
		gbc.gridx = 2;
		gbc.gridy = 7;
		gridBag.setConstraints(textFieldName, gbc);
		contentPane.add(textFieldName);

		// labelQuantity
		gbc.gridx = 1;
		gbc.gridy = 8;
		gridBag.setConstraints(labelQuantity, gbc);
		contentPane.add(labelQuantity);

		// textFieldQuantity
		gbc.gridx = 2;
		gbc.gridy = 8;
		gridBag.setConstraints(textFieldQuantity, gbc);
		contentPane.add(textFieldQuantity);

		// labelCurrentRate
		gbc.gridx = 1;
		gbc.gridy = 9;
		gridBag.setConstraints(labelCurrentRate, gbc);
		contentPane.add(labelCurrentRate);
		
		// textFieldCurrentRate
		gbc.gridx = 2;
		gbc.gridy = 9;
		gridBag.setConstraints(textFieldCurrentRate, gbc);
		contentPane.add(textFieldCurrentRate);
		
		// labelBuyRate
		gbc.gridx = 1;
		gbc.gridy = 10;
		gridBag.setConstraints(labelBuyRate, gbc);
		contentPane.add(labelBuyRate);

		// textFieldBuyRate
		gbc.gridx = 2;
		gbc.gridy = 10;
		gridBag.setConstraints(textFieldBuyRate, gbc);
		contentPane.add(textFieldBuyRate);
		
		// labelBuyDate
		gbc.gridx = 1;
		gbc.gridy = 11;
		gridBag.setConstraints(labelBuyDate, gbc);
		contentPane.add(labelBuyDate);

		// textFieldBuyDate
		gbc.gridx = 2;
		gbc.gridy = 11;
		gridBag.setConstraints(textFieldBuyDate, gbc);
		contentPane.add(textFieldBuyDate);
		
		// labelSold
		gbc.gridx = 1;
		gbc.gridy = 12;
		gridBag.setConstraints(labelSold, gbc);
		contentPane.add(labelSold);
		
		// checkBoxSold
		gbc.gridx = 2;
		gbc.gridy = 12;
		gridBag.setConstraints(checkBoxSold, gbc);
		contentPane.add(checkBoxSold);
		
		// labelSellRate
		gbc.gridx = 1;
		gbc.gridy = 13;
		gridBag.setConstraints(labelSellRate, gbc);
		contentPane.add(labelSellRate);

		// textFieldSellRate
		gbc.gridx = 2;
		gbc.gridy = 13;
		gridBag.setConstraints(textFieldSellRate, gbc);
		contentPane.add(textFieldSellRate);
		
		// labelSellDate
		gbc.gridx = 1;
		gbc.gridy = 14;
		gridBag.setConstraints(labelSellDate, gbc);
		contentPane.add(labelSellDate);

		// textFieldSellDate
		gbc.gridx = 2;
		gbc.gridy = 14;
		gridBag.setConstraints(textFieldSellDate, gbc);
		contentPane.add(textFieldSellDate);
		
		// labelDummySpace
		gbc.gridx = 1;
		gbc.gridy = 15;
		gbc.gridwidth = 2;
		gridBag.setConstraints(labelDummySpace3, gbc);
		contentPane.add(labelDummySpace3);
				
		/*
		 * Buttons
		 */
		// buttonGetName
		gbc.gridx = 3;
		gbc.gridy = 7;
		gridBag.setConstraints(buttonGetName, gbc);
		contentPane.add(buttonGetName);
		
		// buttonGetCurrentRate
		gbc.gridx = 3;
		gbc.gridy = 9;
		gridBag.setConstraints(buttonGetCurrentRate, gbc);
		contentPane.add(buttonGetCurrentRate);
		
		// buttonCurrentBuyDate
		gbc.gridx = 3;
		gbc.gridy = 11;
		gridBag.setConstraints(buttonCurrentBuyDate, gbc);
		contentPane.add(buttonCurrentBuyDate);
		
		// buttonCurrentSellDate
		gbc.gridx = 3;
		gbc.gridy = 14;
		gridBag.setConstraints(buttonCurrentSellDate, gbc);
		contentPane.add(buttonCurrentSellDate);
		
		// buttonSave
		gbc.gridx = 1;
		gbc.gridy = 17;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.FIRST_LINE_START;
		gbc.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(buttonSave, gbc);
		contentPane.add(buttonSave);

		// buttonCancel
		gbc.gridx = 2;
		gbc.gridy = 17;
		gbc.fill = GridBagConstraints.FIRST_LINE_END;
		gbc.anchor = GridBagConstraints.EAST;
		gridBag.setConstraints(buttonCancel, gbc);
		contentPane.add(buttonCancel);
		
		/*
		 * Actions
		 */
		// buttonGetName
		buttonGetName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				// Setzen des aktuellen Datums mit Uhrzeit ins BuyDate Textfeld
				textFieldName.setText(DataProvider.getNameFromWKN(textFieldWkn.getText()));
			}
		});
		
		// buttonGetCurrentRate
		buttonGetCurrentRate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				// Setzen des aktuellen Datums mit Uhrzeit ins BuyDate Textfeld
				textFieldCurrentRate.setText(DataProvider.getCurrentRateFromWKN(textFieldWkn.getText()));
			}
		});
		
		// buttonCurrentBuyDate
		buttonCurrentBuyDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				// Setzen des aktuellen Datums mit Uhrzeit ins BuyDate Textfeld
				textFieldBuyDate.setText(Stock.getCurrentDate());
			}
		});
		
		// buttonCurrentSellDate
		buttonCurrentSellDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				// Setzen des aktuellen Datums mit Uhrzeit ins BuyDate Textfeld
				textFieldSellDate.setText(Stock.getCurrentDate());
			}
		});
		
		// buttonSave
		buttonSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				JFrame frame = null;
				
				// leere Verkaufs-Felder mit Fehler-Meldung abfangen, wenn Verkauft-Checkbox aktiviert 
				if (checkBoxSold.isSelected()) {
					if (textFieldSellRate.getText().isEmpty())
						MessageDialogs.createErrorDialogEmptyFields("Verkaufs-Preis");
					
					if (textFieldSellDate.getText().isEmpty())
						MessageDialogs.createErrorDialogEmptyFields("Verkaufs-Datum");
					
					if (!textFieldSellRate.getText().isEmpty() && !textFieldSellDate.getText().isEmpty()) {
						setModifyStock();
						
						// TODO *DEBUG*
						System.out.println("es wurde eine vorhandene Aktie bearbeitet...");
							
						// Tabelle aktualisieren
						AllStocks.fireTableDataChanged();
							
						setVisible(false);
						dispose();
					}
				} else {

					setModifyStock();
					
					// TODO *DEBUG*
					System.out.println("es wurde eine vorhandene Aktie bearbeitet...");
						
					// Tabelle aktualisieren
					AllStocks.fireTableDataChanged();

					// HashMap aktualisieren
					Chart.updateStockValueHistory(selectedItem);
					Chart.updateDepotValueHistory();
						
					setVisible(false);
					dispose();
				}
			}
		});
		
		// buttonCancel
		buttonCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				//System.exit(0);
				setVisible(false);
				dispose();
			}
		});
		
		return contentPane;
	}
	
	public ModifyStock(String title, int x, int y, int selectedRow) {
		
		this.setTitle(title);
		this.setSize(x, y);
		// zentrale Positionierung des Fensters
		this.setLocationRelativeTo(null);
		
		ModifyStockContainer(selectedRow);
		
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		JComboBox cb = (JComboBox)e.getSource();
		int selectedItem = cb.getSelectedIndex();
		
		// TODO *DEBUG*
		System.out.println("comboBox, selectedItem: " + selectedItem);
		
		this.selectedItem  = selectedItem;
		
		// Aufruf Methode zum "Setten" der Textfelder
		setTextFields();
	}

	/*
	 * Methode zum Setzen der Textfelder beim Wechsel der Combobox
	 */
	public void setTextFields() {		
		Integer getId = list.get(selectedItem).getId();
		textFieldId.setText(getId.toString());
		
		textFieldWkn.setText(list.get(selectedItem).getWkn());
		textFieldName.setText(list.get(selectedItem).getName());
		
		Integer getQuantity = list.get(selectedItem).getQuantity();
		textFieldQuantity.setText(getQuantity.toString());
		
		textFieldCurrentRate.setText(new Double(list.get(selectedItem).getCurRate()).toString());
		
		Double getBuyRate = list.get(selectedItem).getBuyRate();
		textFieldBuyRate.setText(getBuyRate.toString());
		
		textFieldBuyDate.setText(list.get(selectedItem).getBuyDate());
		
		/* 
		 * Fuer Aktien, die noch nicht verkauft worden sind, werden in die Textfelder
		 * leere Strings eingefuegt.
		 */
		if (list.get(selectedItem).isSold()) {
			checkBoxSold.setSelected(true);
			
			Double getSellRate = list.get(selectedItem).getSellRate();
			textFieldSellRate.setText(getSellRate.toString());
			
			textFieldSellDate.setText(list.get(selectedItem).getSellDate()); 
		} else {
			checkBoxSold.setSelected(false);
			
			textFieldSellRate.setText("");
			textFieldSellDate.setText("");
		}
	}
	
	/*
	 * Methode zum Setzen der eingegebenen Werte
	 */
	public void setModifyStock() {
		list.get(selectedItem).setName(textFieldName.getText());
		list.get(selectedItem).setQuantity(new Integer(textFieldQuantity.getText()));
		list.get(selectedItem).setCurRate(new Double(textFieldCurrentRate.getText()));
		list.get(selectedItem).setBuyDate(textFieldBuyDate.getText());
		list.get(selectedItem).setBuyRate(new Double(textFieldBuyRate.getText()));
		
		// Wenn "Verkauft"-Checkbox aktiviert ist, setze Aktie auf "verkauft"
		if (checkBoxSold.isSelected()) {
			list.get(selectedItem).setSold(true);
			list.get(selectedItem).setSellDate(textFieldSellDate.getText());
			list.get(selectedItem).setSellRate(new Double(textFieldSellRate.getText()));
		}
		else {
			list.get(selectedItem).setSold(false);
		}
	}
}
