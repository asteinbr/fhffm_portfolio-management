/**
 * 
 */
package view;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import model.PortfolioManagement;
import model.Stock;
import model.StocksModel;


/**
 * @author alexander
 * 
 * Class - Info
 * 
 * Diese Klasse definiert den "Info"-Dialog.
 */
public class Info extends JFrame {
	public StocksModel instance = StocksModel.getInstance();
	public ArrayList<Stock> list = StocksModel.getInstance().getStocksList();
	
	public Info(String title, int x, int y) {
		
		this.setTitle(PortfolioManagement.applicationName + " v" + PortfolioManagement.applicationVersion);
		this.setSize(x, y);
		// zentrale Positionierung des Fensters
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		// Deklaration Labels und Positionierung
		JLabel labelApplicationName = new JLabel();
			labelApplicationName.setFont(new Font("",Font.BOLD,18));
			labelApplicationName.setText(PortfolioManagement.applicationName);
			labelApplicationName.setAlignmentX(CENTER_ALIGNMENT);
		JLabel labelApplicationVersion = new JLabel();
			labelApplicationVersion.setText("v" + PortfolioManagement.applicationVersion);
			labelApplicationVersion.setAlignmentX(CENTER_ALIGNMENT);
		JLabel labelApplicationDescription = new JLabel();
			labelApplicationDescription.setText(PortfolioManagement.applicatoinDescription);
			labelApplicationDescription.setAlignmentX(CENTER_ALIGNMENT);
		JLabel labelApplicationCopyright = new JLabel();
			labelApplicationCopyright.setFont(new Font("",Font.ITALIC,10));
			labelApplicationCopyright.setText(PortfolioManagement.applicationCopyright);
			labelApplicationCopyright.setAlignmentX(CENTER_ALIGNMENT);
		JLabel labelSpaceDummy1 = new JLabel(" ");
		JLabel labelSpaceDummy2 = new JLabel(" ");
		JLabel labelSpaceDummy3 = new JLabel(" ");
		JLabel labelSpaceDummy4 = new JLabel(" \n");
		JButton buttonClose = new JButton();
			buttonClose.setText("Schließen");
			buttonClose.setAlignmentX(CENTER_ALIGNMENT);
		
		Container contentPane = getContentPane();
		this.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

		contentPane.add(labelSpaceDummy1);
		contentPane.add(labelApplicationName);
		contentPane.add(labelApplicationVersion);
		contentPane.add(labelSpaceDummy2);
		contentPane.add(labelApplicationDescription);
		contentPane.add(labelSpaceDummy3);
		contentPane.add(labelApplicationCopyright);
		contentPane.add(labelSpaceDummy4);
		contentPane.add(buttonClose);
		
		// Action Button "Close / Schließen"
		buttonClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				setVisible(false);
				dispose();
			}
		});
	}
}


