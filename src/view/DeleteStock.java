/**
 * 
 */
package view;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import model.StocksModel;
import model.Stock;


/**
 * @author alexander
 * 
 * Class - DeleteStock
 * 
 * Diese Klasse definiert den "Aktien löschen"-Dialog.
 * Nutzung des GridBag-Layouts.
 */
public class DeleteStock extends JFrame implements ActionListener {
	private ArrayList<Stock> list = StocksModel.getInstance().getStocksList();
	private int selectedItem = 0;
	
	private Container DeleteStockContainer(int selectedRow) {
		// GridBagLayout
		Container contentPane = getContentPane();
		GridBagLayout gridBag = new GridBagLayout();
		contentPane.setLayout(gridBag);
		
		// Deklaration der Labels
		JLabel labelInfo = new JLabel("Zum Löschen Aktie auswählen:");
		//labelInfo.setText("Wählen Sie eine Aktie aus, die gelöscht werden soll:");
		JComboBox comboBox = new JComboBox();
			// ActionListener aktivieren
			comboBox.addActionListener(this);
		
		// Elemente aus der Aktien-Liste der comboBox hinzufügen
		for (int i = 0; i < list.size(); i++)
			comboBox.addItem(list.get(i).getName());

		// ComboBox auf das uebergebene Element setzen
		if (selectedRow >= 0)
			comboBox.setSelectedIndex(selectedRow);
		
		JLabel labelDummySpace1 = new JLabel(" ");
		JLabel labelDummySpace2 = new JLabel(" ");

		// Deklaration der Buttons
		JButton buttonDelete = new JButton("Löschen");
		JButton buttonCancel = new JButton("Abbrechen");
		
		/*
		 * Hinzufügen der einzelnen Elemente
		 * 
		 * Jedes Element braucht einen GridBagConstraint mit Details über
		 * die Positionierung
		 */
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(2, 2, 2, 2);
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.fill = GridBagConstraints.FIRST_LINE_START;
		gbc.anchor = GridBagConstraints.WEST;
		
		// labelInfo
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		gridBag.setConstraints(labelInfo, gbc);
		contentPane.add(labelInfo);

		// labelDummySpace
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridwidth = 3;
		gridBag.setConstraints(labelDummySpace1, gbc);
		contentPane.add(labelDummySpace1);
		
		// comboBox
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.gridwidth = 2;
		gridBag.setConstraints(comboBox, gbc);
		contentPane.add(comboBox);
		
		// labelDummySpace2
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.gridwidth = 3;
		gridBag.setConstraints(labelDummySpace2, gbc);
		contentPane.add(labelDummySpace2);
		
		/*
		 * Buttons
		 */
		// buttonDelete
		gbc.gridx = 1;
		gbc.gridy = 8;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.FIRST_LINE_START;
		gbc.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(buttonDelete, gbc);
		contentPane.add(buttonDelete);

		// buttonCancel
		gbc.gridx = 2;
		gbc.gridy = 8;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.FIRST_LINE_END;
		gbc.anchor = GridBagConstraints.EAST;
		gridBag.setConstraints(buttonCancel, gbc);
		contentPane.add(buttonCancel);
		
		/*
		 * ButtonEvents
		 */
		// buttonDelete
		buttonDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				int ok = MessageDialogs.createQuestionDialog("Aktie Löschen", "Möchten Sie ausgewählte Aktie wirklich löschen?");
				
				if (ok == JOptionPane.YES_OPTION) {					
					// Löschen der ausgewaehlten Aktie
					list.remove(selectedItem);
					
					// Aktualisieren der Tabelle
					AllStocks.fireTableDataChanged();

					// HashMap aktualisieren
					Chart.updateStockValueHistory(selectedItem);
					Chart.updateDepotValueHistory();
					
					setVisible(false);
					dispose();
				}
			}
		});
		
		// buttonCancel
		buttonCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				setVisible(false);
				dispose();
			}
		});
		
		return contentPane;
	}
	
	public DeleteStock(String title, int x, int y, int selectedRow) {
		
		this.setTitle(title);
		this.setSize(x, y);
		// zentrale Positionierung des Fensters
		this.setLocationRelativeTo(null);

		DeleteStockContainer(selectedRow);
		
		this.setVisible(true);		
	}

	/* Methode, die beim Inhaltswechsel der ComboBox automatisch den Index in die
	 * Variable selectedItem schreibt.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		JComboBox cb = (JComboBox)e.getSource();
		int selectedItem = cb.getSelectedIndex();
		System.out.println("comboBox, selectedItem: " + selectedItem);
		this.selectedItem = selectedItem;
	}
}
