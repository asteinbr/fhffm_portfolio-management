/**
 * 
 */
package view;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * @author alexander
 * 
 * Class - MessageDialogs
 * 
 * Diese Klasse generiert Standard Info/Fehler-Meldungen.
 */
public class MessageDialogs {
	
	static JFrame frame = null;
	
	// Fehler-Dialog
	public static void createErrorDialog(String title, String errorMsg) {
		JOptionPane.showMessageDialog(frame, 
				errorMsg, title, JOptionPane.ERROR_MESSAGE);
	}
	
	// Fehler-Dialog für leere Felder
	public static void createErrorDialogEmptyFields(String textField) {
		JOptionPane.showMessageDialog(frame, 
				"Bitte füllen Sie das Feld \"" + textField + "\" aus!", 
				"Fehler", JOptionPane.ERROR_MESSAGE);
	}
	
	// Ja/Nein-Dialog
	public static int createQuestionDialog(String title, String question) {
		return JOptionPane.showConfirmDialog(frame, 
				question, title, JOptionPane.YES_NO_OPTION);
	}	
	
	// Info-Dialog
	public static void createMessageDialog(String title, String message) {
		JOptionPane.showMessageDialog(frame, message, title, JOptionPane.INFORMATION_MESSAGE);
	}
}
