/**
 * 
 */
package model;

import java.util.ArrayList;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

/**
 * @author alexander
 * 
 * Class - TableModelStocks
 * 
 * Diese Klasse definiert das DefaultTableModel für die Anzeige der Aktien.
 */

/*
 * Klasse fuer das Table-Model.
 * Methoden wurden automatisch angelegt, da AbstractTableModel eine abstrakte Klasse ist
 */
public class TableModelStocks extends AbstractTableModel {
	private ArrayList<Stock> list = StocksModel.getInstance().getStocksList();
	
	@Override
	public void addTableModelListener(TableModelListener l) {
	}

	/*
	 * Gibt eine Zelle als String zurueck.
	 * Spalte 7 (Verkauft?) wird als CheckBox in der Tabelle dargestellt,
	 * daher ist der Return-Wert beim columnIndex 7 = Boolean
	 * 
	 * Wenn nur Object zurückgegeben wird, ist die Sortierung nicht korrekt,
	 * daher verwende ich die entsprechenden Rückgabetypen
	 */
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex == 0)
			return Integer.class;		// ID
		else if (columnIndex == 3)
			return Integer.class;		// Quantity
		else if (columnIndex == 4)
			return Double.class;		// current Rate
		else if (columnIndex == 6)
			return Double.class;		// Buy-Rate
		else if (columnIndex == 7)
			return Boolean.class;		// is Sold
		else if (columnIndex == 8)
			return Double.class;		// Sell-Rate
		else
			return String.class;		// others
	}

	/*
	 * Methode gibt an, wieviele Spalten erstellt werden muessen
	 */
	public int getColumnCount() {
		// Gibt die Anzahl der Spalten zurueck plus ein zusätzliches Feld
		// welches fuer ID notwendig ist
		return 10;
	}

	/*
	 * Methode gibt an, wieviele Zeilen die Tabelle benötigt.
	 */
	public int getRowCount() {
		return list.size();
	}

	/*
	 * Methode erteilt den Spalten-Namen. Hierbei wird auf die erste Zeile der 
	 * CSV-Datei zurueckgegriffen.
	 */
	public String getColumnName(int columnIndex) {
		switch (columnIndex) {
			case 0:
				return "ID";
			case 1:
				return "WKN";
			case 2:
				return "Name";
			case 3:
				return "#";
			case 4:
				return "akt. Preis";
			case 5:
				return "K-Datum";
			case 6:
				return "K-Preis";
			case 7:
				return "VK?";
			case 8:
				return "VK-Preis";
			case 9:
				return "Performance";
			default:
				return "";
		}
	}
	
	/*
	 * Methode liefert die Werte einzelner Zellen zurueck
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		Stock selectedRow = list.get(rowIndex);
		switch(columnIndex) {
			case 0:
				return selectedRow.getId();
			case 1:
				return selectedRow.getWkn();
			case 2: 
				return selectedRow.getName();
			case 3:
				return selectedRow.getQuantity();
			case 4:
				return selectedRow.getCurRate();
			case 5:
				return selectedRow.getBuyDate();
			case 6:
				return selectedRow.getBuyRate();
			case 7:
				if(selectedRow.isSold())
					return new Boolean(true);
				else
					return new Boolean(false);
			case 8:
				if(selectedRow.isSold())
					return selectedRow.getSellRate();
			case 9:
				if(selectedRow.isSold()) {
					if (Stock.getSellChange(selectedRow) < 0.0)
						return Stock.getPerformance(selectedRow);
					else if (Stock.getSellChange(selectedRow) > 0.0)
						return Stock.getPerformance(selectedRow);
				}
			default:
				return null;
		}
	}

	/*
	 * Methode ermoeglicht das direkte Editieren der Zellen wenn der Rueckgabewert true ist
	 * Damit innerhalb der Tabelle keine Werte editiert werden koennen, wird hier false zurueckgegeben.
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			default:
				return false;
		}
	}
	
	/*
	 * Methode die als Setter fuer einzelne Zellen gilt, d.h. es ist moeglich die Zelle direkt in der 
	 * Tabelle zu editieren. Funktion ist nur gegeben, wenn die Methode isCellEditable ein 
	 * true zurueckliefert.
	 */
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		Stock selectedRow = list.get(rowIndex);
		
		switch(columnIndex) {
			case 0: 	list.get(rowIndex).setId((Integer) value);
			case 1: 	list.get(rowIndex).setWkn((String) value);
			case 2: 	list.get(rowIndex).setName((String) value);
			case 3: 	list.get(rowIndex).setQuantity((Integer) value);
			case 4: 	list.get(rowIndex).setCurRate((Double) value);
			case 5: 	list.get(rowIndex).setBuyDate((String) value);
			case 6: 	list.get(rowIndex).setBuyRate((Double) value);
			case 7: 	list.get(rowIndex).setSold((Boolean) value);
			case 8: 	list.get(rowIndex).setSellRate((Double) value);
			case 9: 	list.get(rowIndex).getPerformance(selectedRow);
			break;
		}
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
	}
}