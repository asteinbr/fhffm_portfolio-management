/**
 * 
 */
package model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;

import view.MessageDialogs;

/**
 * @author alexander
 * 
 * Class - DataProvider
 * 
 * Diese Klasse beinhaltet Methoden die eine Verbindung zu einer URL herstellen,
 * und den HTML-Quelltext nach bestimmten Dingen durchsuchen.
 */
public class DataProvider {
	/*
	 * Methode zum "crawlen" einer Webseite nach Informationen
	 * In diesem Fall wird die Seite LS-D nach dem Namen zu einer WKN gesucht
	 * 
	 * *Dieses Verfahren ist offiziell nicht gestattet und wurde nur mangels eines Daten-Providers verwendet*
	 */
	public static String getNameFromWKN(String wkn)  {
		HashMap<String, String> valueMap = new HashMap<String, String>();
		
		valueMap.put("Name","");
		
		/*
		 * Öffnen einer HTML-Seite, welcher die WKN angehaengt wird
		 * Speicherung des HTML-Quelltextes erfolgt im BufferedReader br
		 */
		BufferedReader br = null;
		try {
			URL url = new URL("http://www.ls-d.de/Kurs-Details.134.0.html?&tx_sktquotes_pi1[type]=QUOTES&tx_sktquotes_pi1[wkn]=" + wkn);
			br = new BufferedReader(new InputStreamReader(url.openStream()));
		} catch (Exception e) {
			e.printStackTrace();
			MessageDialogs.createErrorDialog("Fehler", "Fehler bei der Anfrage an den Datenprovider.");
		}
		
		/*
		 * Durchsuchen des HTML-Strings nach dem Namen der Aktie
		 */
		try {
			// Temporaere Variable in die, die aktuelle Zeile vom BufferdReadergespeichert wird
			String current_line = "";
			
			// Solange der BufferedReader bereit und nicht am EOF angelangt ist
			while (br.ready()) {
				// aktuelle Zeile wird in current_line gespeichert
				current_line = br.readLine();
				
				// Debug
				//System.out.println(current_line);
				
				// Gehe ueber alle Keys der HashMap
				for(String key: valueMap.keySet()) {
					// Wenn die aktuelle Zeile einen Key enthaelt, splitte ihn und speichere ihn in der HashMap ab
					// Beispiel:	<span>Name:</span>BEIERSDORF<br />
					if(current_line.contains(key))
						valueMap.put(key, current_line.split("</span>")[1].split("<br />")[0]);	
				}
			}
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			aioobe.printStackTrace();
			MessageDialogs.createErrorDialog("Fehler", "Fehler bei der Anfrage an den Datenprovider.\nÜberprüfen Sie die WKN.");
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return valueMap.get("Name");
	}
	
	/*
	 * Methode ähnlich wie obige
	 */
	public static String getCurrentRateFromWKN(String wkn)  {
		HashMap<String, String> valueMap = new HashMap<String, String>();
		
		valueMap.put("ASK","");
		
		BufferedReader br=null;
		try
		{
			URL url = new URL("http://www.ls-d.de/Kurs-Details.134.0.html?&tx_sktquotes_pi1[type]=QUOTES&tx_sktquotes_pi1[wkn]=" + wkn);
			br = new BufferedReader(new InputStreamReader(url.openStream()));
		}
		catch ( Exception e ) {
			e.printStackTrace();
		}
		
		try {
			String current_line = "";
			
			while (br.ready()) {
				current_line = br.readLine();
				
				for(String key: valueMap.keySet()) {
					if(current_line.contains(key))
						valueMap.put(key, current_line.split("</span>")[1].split("<br />")[0]);	
				}
			}
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			aioobe.printStackTrace();
			MessageDialogs.createErrorDialog("Fehler", "Fehler bei der Anfrage an den Datenprovider.\nÜberprüfen Sie die WKN.");
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return valueMap.get("ASK");
	}
}
