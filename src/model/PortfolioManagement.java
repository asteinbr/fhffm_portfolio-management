package model;

/**
 * @author alexander
 * 
 * Class - PortfolioManagement
 * 
 * Diese Klasse beinhaltet Informationen über die Anwendung
 */

public class PortfolioManagement {
	public static double applicationVersion = 0.5;
	public static String applicationName = "Portfolio-Management";
	public static String applicatoinDescription = "A tool to manage your portfolio.";
	public static String applicationCopyright = "© 2010-2011 Alexander Steinbrecher";
}
